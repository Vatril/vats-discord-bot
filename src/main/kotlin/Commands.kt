import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.DiscordException
import sx.blah.discord.util.EmbedBuilder
import java.util.Random
import kotlin.math.roundToLong
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import sx.blah.discord.handle.obj.Permissions


val commandhandlers = listOf(
        Help(),
        Ping(),
        RollDice(),
        E621(true),
        E621(false),
        Purge()
)

enum class Category {
    FUN, UTIL, ADMIN
}


interface CommandHandler : MessageHandler {
    val command: String
    val summary: String
    val help: String
    val category: Category

    fun handleMessage(event: MessageReceivedEvent)

    override fun handle(event: MessageReceivedEvent): Boolean {
        handleMessage(event)
        return true
    }
}

interface AdminCommandHandler : CommandHandler {
    override fun handle(event: MessageReceivedEvent): Boolean {
        if (event.author.getPermissionsForGuild(event.guild).contains(Permissions.ADMINISTRATOR)) {
            handleMessage(event)
        } else {
            event.message.reply("you don't have permission to do that!")
        }
        return true
    }
}

class Help : CommandHandler {

    override val command = "help"
    override val summary = "Prints this!"
    override val help = "use either `./help` alone or with another command behind it. "
    override val category = Category.UTIL

    override fun handleMessage(event: MessageReceivedEvent) {

        val parts = event.message.content.split(" ")
        if (parts.size > 1) {
            for (handler in commandhandlers) {
                if (parts[1].startsWith(handler.command)) {
                    event.message.channel.sendMessage("${parts[1]}:\n${handler.help}")
                    return
                }
            }
            event.message.channel.sendMessage("Cant't find ${parts[1]}!")
            return
        }
        var funhelp = ""
        var utilhelp = ""
        var adminhelp = ""
        commandhandlers.forEach {
            when (it.category) {
                Category.UTIL -> utilhelp += "`./${it.command}` => ${it.summary}\n"
                Category.FUN -> funhelp += "`./${it.command}` => ${it.summary}\n"
                Category.ADMIN -> adminhelp += "`./${it.command}` => ${it.summary}\n"
            }
        }
        event.message.channel.sendMessage(
                EmbedBuilder()
                        .withColor(0xFF00FF)
                        .withTitle("Here are all commands:")
                        .appendField("Fun", funhelp, false)
                        .appendField("Util", utilhelp, false)
                        .appendField("Admin", adminhelp, false)
                        .appendField("For details:", "`./help <command>`", false)
                        .build()
        )
    }

}

class Ping : CommandHandler {

    override val command = "ping"
    override val summary = "Measures the time it took for your message to reach this bot."
    override val help = "write ./ping"
    override val category = Category.UTIL

    override fun handleMessage(event: MessageReceivedEvent) {
        event.message.reply(
                "got the message after ${System.currentTimeMillis() -
                        (event.message.timestamp.epochSecond * 1000)}ms!")
    }

}

class RollDice : CommandHandler {

    private class DiceRoll(val numberOfDice: Int, val maxValueOfDie: Int) {

        val result: Long
        val individualResult = mutableListOf<Long>()

        init {
            val r = Random()
            for (i in 1..numberOfDice) {
                individualResult += (r.nextInt(maxValueOfDie) + 1).toLong()
            }
            result = individualResult.sum()
        }

        companion object {

            fun fromString(parameters: String): DiceRoll? {
                val parts = parameters.split("d")
                if (parts.size == 2) {
                    val nod = parts[0].toIntOrNull()
                    val mod = parts[1].toIntOrNull()
                    if (nod != null && mod != null && nod <= 1000) {
                        return DiceRoll(nod, mod)
                    }
                }
                return null
            }
        }

        override fun toString() = "${numberOfDice}d$maxValueOfDie"

    }

    override val command = "roll"
    override val help = "Syntax: `<number of dice>d<max value of die>`"
    override val summary = "Rolls one or more dice."
    override val category = Category.FUN

    override fun handleMessage(event: MessageReceivedEvent) {
        val parts = event.message.content.split(" ")
        println(parts)
        val dice = mutableListOf<DiceRoll>()
        for (part in parts) {
            DiceRoll.fromString(part).takeIf { it != null }?.also { dice.add(it) }
        }
        if (dice.size <= 0) {
            event.message.reply("Sorry, couldn't read any dice throws there.\n" +
                    "Are you sure you used the correct syntax?\n$help")
            return
        }
        var result = ""
        for (die in dice) {
            result += "$die: ${die.individualResult.joinToString(separator = ", ")} => ${die.result}\n"
        }

        result += "The total is ${dice.sumByDouble { it.result.toDouble() }.roundToLong()}"
        try {
            event.message.channel.sendMessage(result)
        } catch (exc: DiscordException) {
            event.message.reply(" sorry, something went wrong.\n" +
                    "Maybe this was too complicated.")
        }
    }

}

class E621(private val sfw: Boolean) : CommandHandler {

    override val command = if (sfw) "e926" else "e621"
    override val summary = "Gets a image from ${if (sfw) "e926" else "e621"}."
    override val help = "use `./${if (sfw) "e926" else "e621"} `<query>`"
    override val category = Category.FUN

    override fun handleMessage(event: MessageReceivedEvent) {
        val client = OkHttpClient()

        if (!sfw && !event.message.channel.isNSFW) {
            event.message.channel.sendMessage("You can't use this in a sfw channel!\nUSe e926 instead!")
            return
        }

        val request = Request.Builder()
                .url("https://${if (sfw) "e926" else "e621"}.net/post/index.json?tags=" +
                        event.message.content.split(" ").asSequence().drop(1).joinToString("+"))
                .get()
                .addHeader("User-Agent", e621)
                .build()

        val response = JSONArray(client.newCall(request).execute().body()?.string())
        val post = response[Random().nextInt(response.length())] as JSONObject
        var title = post.getString("description")
        if (title.length > 256)
            title = title.substring(0, 250) + "(...)"
        else if (title.isEmpty())
            title = "No Title"
        event.message.channel.sendMessage(
                EmbedBuilder()
                        .withTitle(title)
                        .appendField("Tags", post.getString("tags").split(" ")
                                .joinToString("\n"), false)
                        .appendField("Rating", post.getString("rating"), true)
                        .appendField("Score", post.getInt("score").toString(), true)
                        .appendField("Artist", post.getJSONArray("artist")
                                .joinToString(separator = " and ")
                                { it.toString().replace("\"", "") }, true)
                        .withImage(post.getString("sample_url"))
                        .withUrl("https://e621.net/post/show/${post.getLong("id")}")
                        .withColor(0x123355)
                        .build())
    }

}

class Purge : AdminCommandHandler {

    override val command = "purge"
    override val summary = "Remove a lot of messages at once."
    override val help = "Use either `./purge @user` or `./purge <number>`"
    override val category = Category.ADMIN

    override fun handleMessage(event: MessageReceivedEvent) {
        if (event.message.mentions.size > 0) {
            for (user in event.message.mentions) {
                event.channel.bulkDelete(event.channel.fullMessageHistory.filter { it.author.equals(user) })
            }
            event.message.channel.sendMessage("Deleted all messages from " +
                    "${event.message.mentions.map { it.name }.joinToString(separator = " and ")}.")
        }else{
            val num = event.message.content.split(" ")[1].toIntOrNull()
            if (num != null){
                event.channel.bulkDelete(event.channel.fullMessageHistory.subList(0, num))
                event.message.channel.sendMessage("Deleted $num messages.")
            }else{
                event.channel.sendMessage("Can't interpret that.\n$help")
            }
        }
    }

}