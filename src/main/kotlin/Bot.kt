import sx.blah.discord.util.DiscordException
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.api.events.IListener
import sx.blah.discord.handle.impl.events.ReadyEvent
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.handle.obj.StatusType


fun main(args:Array<String>){
    val bot = createClient(token ) ?:
    throw Exception("Can't initialize bot")
    registerCommonEvents(bot.dispatcher)
    bot.dispatcher.registerListener(MessageListener())
}

class MessageListener : IListener<MessageReceivedEvent> {

    override fun handle(event: MessageReceivedEvent?) {
        if (event == null || event.message.author.isBot) return

        if (event.message.content.startsWith("./")){
            commandhandlers.forEach {
                if(event.message.content.startsWith("./${it.command}")){
                    it.handleMessage(event)
                    return
                }
            }
        }
        noncommandhandlers.forEach {
            if (it.handle(event)) return
        }
    }

}

interface MessageHandler{
    fun handle(event:MessageReceivedEvent):Boolean
}

fun createClient(token: String): IDiscordClient? {
    val clientBuilder = ClientBuilder()
    clientBuilder.withToken(token)
    return try {
        clientBuilder.login()
    } catch (e: DiscordException) {
        e.printStackTrace()
        null
    }

}