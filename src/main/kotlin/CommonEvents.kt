
import sx.blah.discord.api.events.EventDispatcher
import sx.blah.discord.handle.impl.events.ReadyEvent
import sx.blah.discord.api.events.IListener
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent
import sx.blah.discord.handle.obj.StatusType


fun registerCommonEvents(dispatcher: EventDispatcher){
    dispatcher.registerListeners(
            ReadyListener(),
            NewUserListener(),
            LeaveUserListener()
    )
}

class ReadyListener : IListener<ReadyEvent> {
    override fun handle(event: ReadyEvent) {
        event.client.changeStreamingPresence(StatusType.ONLINE, "use ./help for help", null)
    }
}

class NewUserListener : IListener<UserJoinEvent> {
    override fun handle(event: UserJoinEvent?) {
        event?.guild?.defaultChannel?.sendMessage(
                "Welcome ${event.user?.mention()}")
        event?.user?.orCreatePMChannel?.sendMessage("Welcome to ${event.guild.name}!\n")
    }
}

class LeaveUserListener : IListener<UserLeaveEvent> {
    override fun handle(event: UserLeaveEvent?) {
        event?.guild?.defaultChannel?.sendMessage(
                "Goodbye ${event.user?.name}")
    }
}